/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dannylowpass
 */
public class LibrarySim {
    
    public static void main(String[] args){
        
        Book b1 = new Book("EffectiveJ Java", "Joshua Bloch");
        Book b2 = new Book("Thinking in Java", "Bruce Eckel");
        Book b3 = new Book("Java: The Complete reference", "Herbert Schildt");
        Book b4 = new Book("Ideas for Java Book Titles", "Danny Lopez");
        
        List<Book> books = new ArrayList<Book>();
        books.add(b1);
        books.add(b2);
        books.add(b3);
        books.add(b4);
        
        Library library = new Library(books);
        
        List<Book> bks = library.getTotalBooksInLibrary();
        for(Book bk : bks){
            
            System.out.println("Title: " + bk.title + " and Author: "
                    + bk.author );
        } 
    }
}
